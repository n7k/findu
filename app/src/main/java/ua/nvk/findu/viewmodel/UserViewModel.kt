package ua.nvk.findu.viewmodel

class UserViewModel {
    constructor()

    val designation: String = "User"
    var userId: Int = 0
    var coordinates: String = ""

    constructor(userNum: Int, coordinates: String) {
        this.userId = userNum
        this.coordinates = coordinates
    }

    override fun toString(): String {
        return "UserViewModel(userId=$userId, coordinates=$coordinates)"
    }
}