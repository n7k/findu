package ua.nvk.findu.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ua.nvk.findu.databinding.RecyclerViewItemBinding
import ua.nvk.findu.viewmodel.UserViewModel

class RecyclerViewAdapter(private var userList: MutableList<UserViewModel?>,
                          private val listener: OnItemClickListener?)
    : RecyclerView.Adapter<RecyclerViewAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val binding = RecyclerViewItemBinding.inflate(LayoutInflater.from(parent.context),
                parent, false)
        return UserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) =
            holder.bind(userList[position]!!, listener)

    class UserViewHolder(private val userBinding: RecyclerViewItemBinding)
        : RecyclerView.ViewHolder(userBinding.root) {
        fun bind(user: UserViewModel, listener: OnItemClickListener?) {
            userBinding.userViewModel = user
            if (listener != null) {
                userBinding.root.setOnClickListener { _ -> listener.onItemClick(layoutPosition) }
            }
            userBinding.executePendingBindings()
        }
    }

    override fun getItemCount(): Int = userList.size

    fun setData(userList: MutableList<UserViewModel?>) {
        this.userList.clear()
        this.userList.addAll(userList)
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }
}