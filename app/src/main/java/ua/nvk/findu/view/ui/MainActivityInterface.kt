package ua.nvk.findu.view.ui

import android.view.View

interface MainActivityInterface {
    fun initMap()
    fun initRecycler()
    fun deleteMarkers(v: View)
}