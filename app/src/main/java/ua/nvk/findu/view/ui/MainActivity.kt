package ua.nvk.findu.view.ui

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.google.android.gms.maps.SupportMapFragment
import kotlinx.android.synthetic.main.activity_main.*
import ua.nvk.findu.R
import ua.nvk.findu.firebase.database.Database
import ua.nvk.findu.firebase.map.GoogleMapProvider
import ua.nvk.findu.view.adapter.RecyclerViewAdapter

class MainActivity : AppCompatActivity(), MainActivityInterface, RecyclerViewAdapter.OnItemClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback {

    private lateinit var googleMapProvider: GoogleMapProvider
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initMap()
        initRecycler()
    }

    override fun initMap() {
        val mapFragment: SupportMapFragment? = supportFragmentManager
                .findFragmentById(R.id.googleMap) as? SupportMapFragment
        mapFragment?.getMapAsync {
            googleMapProvider = GoogleMapProvider(it, this, this)
            googleMapProvider.permissionCheck()
            Database.syncData(this, adapter, googleMapProvider)
        }
    }

    override fun initRecycler() {
        usersRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(mutableListOf(), this)
        usersRecyclerView.adapter = adapter
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == GoogleMapProvider.LOCATION_PERMISSION_REQUEST_CODE)
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                googleMapProvider.findCurrentLocation()
            }
    }

    override fun onItemClick(position: Int) {
        Database.findElement(position, this, object : Database.FindElementCallBack {
            override fun onElementFound(userLatLng: String?) {
                googleMapProvider.findUserLocation(userLatLng)
            }
        })
    }

    override fun deleteMarkers(v: View) {
        googleMapProvider.deleteMarkers()
        Toast.makeText(this, R.string.markers_are_deleted, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        Database.deleteSyncDataEventListener()
        super.onDestroy()
    }
}