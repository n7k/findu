package ua.nvk.findu.firebase.database

import android.content.Context
import ua.nvk.findu.firebase.map.GoogleMapProvider
import ua.nvk.findu.view.adapter.RecyclerViewAdapter

interface DatabaseManipulator {
    fun removeList(function: () -> Unit)
    fun syncData(context: Context, rvAdapter: RecyclerViewAdapter,
                 googleMapProvider: GoogleMapProvider)

    fun deleteSyncDataEventListener()
}