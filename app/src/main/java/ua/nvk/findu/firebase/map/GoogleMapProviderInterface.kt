package ua.nvk.findu.firebase.map

interface GoogleMapProviderInterface {
    fun permissionCheck()
    fun findCurrentLocation()
    fun findUserLocation(userLatLng: String?)
}