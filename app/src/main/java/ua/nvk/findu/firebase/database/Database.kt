package ua.nvk.findu.firebase.database

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.*
import ua.nvk.findu.R
import ua.nvk.findu.firebase.map.GoogleMapProvider
import ua.nvk.findu.view.adapter.RecyclerViewAdapter
import ua.nvk.findu.viewmodel.UserViewModel

object Database : DatabaseManipulator, DatabaseElementManipulator {

    const val PREFERENCES = "preferences"
    const val MY_ID = "my_id"
    const val TAG = "ERROR"
    private var userList = mutableListOf<UserViewModel?>()
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var databaseReference: DatabaseReference
    private lateinit var valueEventListener: ValueEventListener
    private lateinit var syncDataEventListener: ValueEventListener

    private val firebase: FirebaseDatabase by lazy {
        val database = FirebaseDatabase.getInstance()
        database.setPersistenceEnabled(true)
        database
    }

    override fun addElement(element: UserViewModel) {
        val reference = firebase.getReference("users")
        reference.push().setValue(element)
    }

    override fun updateElement(dataSnapshot: DataSnapshot, reference: DatabaseReference,
                               sharedPreferences: SharedPreferences, currentLatLng: LatLng) {
        for (element in dataSnapshot.children) {
            if (sharedPreferences.getInt(MY_ID, 0) ==
                    element.getValue(UserViewModel::class.java)?.userId) {
                reference.child(element.key!!)
                        .setValue(UserViewModel(sharedPreferences.getInt(MY_ID, 0),
                                currentLatLng.toString()))
            }
        }
    }

    override fun addOrUpdateElement(context: Context, currentLatLng: LatLng) {
        sharedPreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        val reference = firebase.getReference("users")
        valueEventListener = reference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, context.resources.getString(R.string.failed_to_read_value),
                        p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {
                userList.clear()
                p0.children.forEach { element ->
                    userList.add(element.getValue(UserViewModel::class.java))
                }
                if (sharedPreferences.getInt(MY_ID, 0) == 0) {
                    editor.putInt(MY_ID, userList.size + 1)
                    editor.apply()
                    addElement(UserViewModel(userList.size + 1, currentLatLng.toString()))
                } else {
                    updateElement(p0, reference, sharedPreferences, currentLatLng)
                }
                reference.removeEventListener(valueEventListener)
            }
        })
    }

    override fun findElement(position: Int, context: Context, findElementCallBack: FindElementCallBack) {
        val reference = firebase.getReference("users")
        valueEventListener = reference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, context.resources.getString(R.string.failed_to_read_value),
                        p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {
                for (element in p0.children) {
                    if ((position + 1) == element.getValue(UserViewModel::class.java)?.userId) {
                        findElementCallBack.onElementFound(element
                                .getValue(UserViewModel::class.java)?.coordinates)
                    }
                }
                reference.removeEventListener(valueEventListener)
            }
        })
    }

    override fun removeList(function: () -> Unit) {
        val reference = firebase.getReference("users")
        reference.removeValue { _, _ -> function() }
    }

    override fun syncData(context: Context, rvAdapter: RecyclerViewAdapter, googleMapProvider: GoogleMapProvider) {
        databaseReference = firebase.getReference("users")
        syncDataEventListener = databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, context.resources.getString(R.string.failed_to_read_value),
                        p0.toException())
            }

            override fun onDataChange(p0: DataSnapshot) {
                userList.clear()
                p0.children.forEach { element ->
                    userList.add(element.getValue(UserViewModel::class.java))
                }
                googleMapProvider.placeMarkers(userList)
                rvAdapter.setData(userList)
            }
        })
    }

    override fun deleteSyncDataEventListener() {
        databaseReference.removeEventListener(syncDataEventListener)
    }

    interface FindElementCallBack {
        fun onElementFound(userLatLng: String?)
    }
}