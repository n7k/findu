package ua.nvk.findu.firebase.map

import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.support.v4.app.ActivityCompat
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import ua.nvk.findu.firebase.database.Database
import ua.nvk.findu.view.ui.MainActivity
import ua.nvk.findu.viewmodel.UserViewModel
import java.util.*

class GoogleMapProvider(private val googleMap: GoogleMap,
                        private val context: Context,
                        private val mainActivity: MainActivity) : GoogleMapProviderInterface,
        GoogleMapProviderSetUp {
    companion object { const val LOCATION_PERMISSION_REQUEST_CODE = 77 }

    private val geocoder: Geocoder = Geocoder(context, Locale.getDefault())

    override fun permissionCheck() {
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mainActivity,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
        } else if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            findCurrentLocation()
        }
    }

    override fun findCurrentLocation() {
        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.isMyLocationEnabled = true
        LocationServices.getFusedLocationProviderClient(mainActivity)
                .lastLocation.addOnSuccessListener(mainActivity) { location ->
            if (location != null) {
                val currentLatLng = LatLng(location.latitude, location.longitude)
                setUpFocus(currentLatLng, location)
                Database.addOrUpdateElement(context, currentLatLng)
            }
        }
    }

    override fun findUserLocation(userLatLng: String?) {
        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.addMarker(MarkerOptions().position(stringToLatLng(userLatLng!!))
                .title(makeAddress(userLatLng)))
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(stringToLatLng(userLatLng), 10f))
    }

    override fun setUpFocus(locationLatLng: LatLng, location: Location) {
        googleMap.addMarker(MarkerOptions().position(locationLatLng)
                .title(makeAddress(location.latitude, location.longitude)))
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locationLatLng, 10f))
    }

    override fun makeAddress(latitude: Double, longitude: Double): String {
        val addresses: List<Address> = geocoder.getFromLocation(latitude, longitude, 1)
        return addresses[0].getAddressLine(0)
    }

    override fun makeAddress(userLatLng: String): String {
        val latLng = userLatLng.split(",".toRegex())
                .dropLastWhile { it.isEmpty() }.toTypedArray()
        val addresses: List<Address> = geocoder.getFromLocation(java.lang.Double
                .parseDouble(latLng[0].substring(10, latLng[0].length - 1)),
                java.lang.Double.parseDouble(latLng[1].substring(0, latLng[1].length - 2)), 1)
        return addresses[0].getAddressLine(0)
    }

    override fun stringToLatLng(userLatLng: String): LatLng {
        val latLng = userLatLng.split(",".toRegex())
                .dropLastWhile { it.isEmpty() }.toTypedArray()
        return LatLng(java.lang.Double.parseDouble(latLng[0].substring(10, latLng[0].length - 1)),
                java.lang.Double.parseDouble(latLng[1].substring(0, latLng[1].length - 2)))
    }

    override fun placeMarkers(userList: MutableList<UserViewModel?>) {
        googleMap.clear()
        for (user in userList) {
            googleMap.addMarker(MarkerOptions().position(stringToLatLng(user?.coordinates!!))
                    .title(makeAddress(user.coordinates)))
        }
    }

    override fun deleteMarkers() {
        googleMap.clear()
    }
}