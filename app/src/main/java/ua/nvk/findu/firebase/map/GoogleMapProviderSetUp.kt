package ua.nvk.findu.firebase.map

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import ua.nvk.findu.viewmodel.UserViewModel

interface GoogleMapProviderSetUp {
    fun setUpFocus(locationLatLng: LatLng, location: Location)
    fun makeAddress(latitude: Double, longitude: Double): String
    fun makeAddress(userLatLng: String): String
    fun stringToLatLng(userLatLng: String): LatLng
    fun placeMarkers(userList: MutableList<UserViewModel?>)
    fun deleteMarkers()
}