package ua.nvk.findu.firebase.database

import android.content.Context
import android.content.SharedPreferences
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseReference
import ua.nvk.findu.viewmodel.UserViewModel

interface DatabaseElementManipulator {
    fun addElement(element: UserViewModel)
    fun updateElement(dataSnapshot: DataSnapshot, reference: DatabaseReference,
                      sharedPreferences: SharedPreferences, currentLatLng: LatLng)

    fun addOrUpdateElement(context: Context, currentLatLng: LatLng)
    fun findElement(position: Int, context: Context,
                    findElementCallBack: Database.FindElementCallBack)
}